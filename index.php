<?php
    include 'includes/conexion.php';
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <?php require 'extensiones/head.php'?>
    <title>Registro de Usuarios</title>
</head>
<body style="background: #dfdfdf;">
    <div class="contenedor">
        <div class="titulo">
            <h3>Registro de Usuarios</h3>
            <hr>
        </div>

    <div class="cuerpo">
        <form action="agregar_usuario.php" method="POST">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <span>Nombre:</span>
                        <input type="text" class="form-control" id="nombre" name="nombre">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <span>Apellido Paterno:</span>
                        <input type="text" class="form-control" id="paterno" name="paterno">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <span>Apellido Marterno:</span>
                        <input type="text" class="form-control" id="materno" name="materno">
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <span>Correo Electrónico:</span>
                        <input type="email" class="form-control" id="correo" name="correo">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <span>Teléfono:</span>
                        <input type="number" class="form-control" id="telefono" name="telefono">
                    </div>
                </div>

                <div class="col-md-2">
                    <div class="form-group">
                        <span>Edad:</span>
                        <input type="number" class="form-control" id="edad" name="edad">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <span>Contraseña:</span>
                        <input type="password" class="form-control" id="pass" name="pass">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <span>Domicilio:</span>
                        <input type="text" class="form-control" id="domicilio" name="domicilio">
                    </div>
                </div>

                <div class="col-md-12">
                    <button type="submit" class="btn btn-block" style="background: #EF4824; color: white">Registrarme</button>
                </div>

            </div>
        </form>

        <div class="row">
            <div class="col-md-12">
                <?php
                    if(!empty($_GET['error'])){
                        $respuesta = $_GET['error'];
                        $contenido = $_GET['contenido']; 
                ?>
            
                <?php if($respuesta == 'vacio'){ ?>
                    
                    <div class="col-md-12">
                        <div class="alert alert-success">
                            <?php echo $contenido ?>
                        </div>
                    </div>
                
                <?php } ?>
                <?php } ?>
            </div>
        </div>
    </div>
</div>

   <?php require 'extensiones/scrips.php' ?>
</body>
</html>