<?php include 'includes/conexion.php';

$correo = $_GET['user'];

$sql = "SELECT * FROM usuario WHERE correo='$correo'";
$resultado = $conexion->query($sql);
$resultado = $resultado->fetch_assoc();






?>

<!DOCTYPE html>
<html lang="en">
<head>
    <?php require 'extensiones/head.php'?>
    <title>Modificar Usuario</title>
</head>
<body style="background: #dfdfdf;">
    <div class="contenedor">
        <div class="titulo">
            <h3>Modificar Usuario</h3>
            <hr>
        </div>

    <div class="cuerpo">
        <form action="editar-usuario.php" method="POST">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <span>Nombre:</span>
                        <input type="text" class="form-control" id="nombre" name="nombre" value="<?php echo $resultado['nombre'] ?>">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <span>Apellido Paterno:</span>
                        <input type="text" class="form-control" id="paterno" name="paterno" value="<?php echo $resultado['paterno'] ?>">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <span>Apellido Marterno:</span>
                        <input type="text" class="form-control" id="materno" name="materno" value="<?php echo $resultado['materno'] ?>">
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <span>Correo Electrónico:</span>
                        <input type="email" class="form-control" id="correo" name="correo" readonly>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <span>Teléfono:</span>
                        <input type="number" class="form-control" id="telefono" name="telefono" value="<?php echo $resultado['telefono'] ?>">
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <span>Edad:</span>
                        <input type="number" class="form-control" id="edad" name="edad" value="<?php echo $resultado['edad'] ?>"> 
                    </div>
                </div>
                
                <div class="col-md-8">
                    <div class="form-group">
                        <span>Domicilio:</span>
                        <input type="text" class="form-control" id="domicilio" name="domicilio" value="<?php echo $resultado['domicilio'] ?>">
                    </div>
                </div>

                <div class="col-md-12">
                    <button type="submit" class="btn btn-block" style="background: #EF4824; color: white">Modificar</button>
                </div>

            </div>
        </form>

        <div class="row">
            <div class="col-md-12">
                <?php
                    if(!empty($_GET['error'])){
                        $respuesta = $_GET['error'];
                        $contenido = $_GET['contenido']; 
                ?>
            
                <?php if($respuesta == 'vacio'){ ?>
                    
                    <div class="col-md-12">
                        <div class="alert alert-success">
                            <?php echo $contenido ?>
                        </div>
                    </div>
                
                <?php } ?>
                <?php } ?>
            </div>
        </div>
    </div>
</div>

   <?php require 'extensiones/scrips.php' ?>
</body>
</html>